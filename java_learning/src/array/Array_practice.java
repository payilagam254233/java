package array;

public class Array_practice {

	public static void main(String[] args) {
		Array_practice ap = new Array_practice();
		ap.type();

	}

	private void type() {
		int[] marks = { 65, 55, 69, 69, 80 };
		int total = 0;
		int index = 0;
		while (index < marks.length) {
			System.out.print(marks[index] + " ");
			total = total + marks[index];
			index++;
		}
		System.out.println();
		System.out.print("TOTAL--->" + total);
		System.out.println();
		System.out.print("avg--->" + total / marks.length);
		System.out.println();
		if (total / marks.length > 90 && total / marks.length <= 100) {
			System.out.print("grade--->" + "O+");
		} else if (total / marks.length > 80 && total / marks.length < 90) {
			System.out.println("grade--->" + "O");

		} else if (total / marks.length > 70 && total / marks.length < 80) {
			System.out.println("grade--->" + "A++");
		} else if (total / marks.length > 60 && total / marks.length < 70) {
			System.out.println("grade--->" + "A+");
		} else if (total / marks.length > 50 && total / marks.length < 60) {
			System.out.println("grade--->" + "A");
		} else if (total / marks.length > 40 && total / marks.length < 50) {
			System.out.println("grade--->" + "B+");
		} else if (total / marks.length > 35 && total / marks.length < 40) {
			System.out.println("grade--->" + "B");
		} else if (total / marks.length < 35) {
			System.out.println("grade--->" + "C");
			System.out.println("below 35 fail");
		}

	}

}
