package array;

public class Array_learn {

	public static void main(String[] args) {
		Array_learn al = new Array_learn();
		al.ques1();
		al.ques2();
		al.ques3();
		al.ques4();
		al.ques6();
		al.ques7();
		al.ques8();
		al.ques9();

		
	}

	private void ques9() {
		int[] no = {10,20,30,40,50};
		for(int value=no.length-1;value>=0;value--) {
			System.out.print(no[value]+" ");
		}
	System.out.println();
	}

	private void ques8() {
		int[] no = {10,20,30,40,50};
		for (int i : no) {
			System.out.print(i+" ");
		}
		System.out.println();
		
	}

	private void ques7() {
		int[] score = {88,75,90,89,76};
		int total = 0;
		int no = 0;
		while(no<score.length) {
			System.out.print(score[no]+" ");
			total = total+score[no];
			no++;
		}
		System.out.println();
		System.out.print("TOTAL--->"+total);
		System.out.println();
	}

	private void ques6() {
		int[] no = {98,34,56,78,27};
		int count = 0;
		while(count<no.length) {
			if(count<35) {
				System.out.print(count+" ");
				count++;
			}
		}
		
		System.out.println();
	}

	private void ques5() {
		int no=1;
		while(no<=5) {
		for(int col=1;col<=5;col++) {
			System.out.print(no+" ");
		}
		System.out.println();
		no++;
		}
		System.out.println();
	}

	private void ques4() {
		int[] no = {1,2,3,4,5};
		int check = 0;
		while(check<no.length) {
			System.out.print(no[check]+" ");
			check++;
		}
		System.out.println();
	}

	private void ques3() {
		int[] days = {1,2,3,4,5,6,7};
		int no = 1;
		while(no<=days.length) {
			if(no==days[0]) {
				System.out.println("sunday");
			}
			else if(no==days[1]) {
				System.out.println("monday");
			}
			else if(no==days[2]) {
				System.out.println("tuesday");
			}
			else if(no==days[3]) {
				System.out.println("wednesday");
			}
			else if(no==days[4]) {
				System.out.println("thursday");
			}
			else if(no==days[5]) {
				System.out.println("friday");
			}
			else if(no==days[6]) {
				System.out.println("saturday");
			}
			no++;
		}
		System.out.println();
	}

	private void ques2() {
		int[] score= {55,55,56,54,49};
		int marks=0;
		while(marks<score.length) {
			if(score[marks]<50) {
				System.out.println("fail");
			}
			else {
				System.out.println("pass");
			}
			marks++;
		}
		
		System.out.println();
	}
	private void ques1() {
		int no = 44;
		if(no%2==0) {
			System.out.println("even");
		}
		else {
			System.out.println("odd");
		}
		System.out.println();
	}

}
