package learn_regex;

import java.util.regex.*;

public class Regex_demo1 {

	public static void main(String[] args) {
		// String input = "My$$%% mobile number is 6383774571";

		// Pattern patternObj = Pattern.compile("\\d{10}");//-->"\\d{10}" -count for
		// seeing number.for printing group numbers.
		// Pattern patternObj = Pattern.compile("[0123456789]");//--> to print the
		// sequence of ungrouped number also.
		// Pattern patternObj = Pattern.compile("[0-9]");//-->a method also print the
		// ungrouped numbers.
		// Pattern patternObj = Pattern.compile("[a-z]");//-->to print small letters.
		// Pattern patternObj = Pattern.compile("[A-Z]");//-->to print capital letters.
		// Pattern patternObj = Pattern.compile("[A-Za-z0-9 ]");-->to print those all.
		// Pattern patternObj = Pattern.compile("[a-zA-Z ]");//-->to print the all spaces,skip special characters.
		// Pattern patternObj = Pattern.compile("[^a-zA-Z0-9]");//-->to print the all spaces.

//		Matcher matcherObj = patternObj.matcher(input);
//		while (matcherObj.find()) {
//			System.out.println(matcherObj.group());
//			System.out.println(matcherObj.start());
//			System.out.println(matcherObj.end());
//		}

		
		
		
		
		
//		String password = "Chennai is the capital of TamilNadu";
//		  
//			//Pattern patternObj = Pattern.compile("^Chennai");-->to print the first word of sentence.
//			Pattern patternObj = Pattern.compile("TamilNadu$");//-->$-ends with symbol.
//		    Matcher matcherObj = patternObj.matcher(password);
//		    while(matcherObj.find())
//		    {
//		      System.out.print(matcherObj.group());
//		    }
		
		
		
		

		//String password = "Chennai is the capital city 000111";
		// Pattern patternObj = Pattern.compile("\\s");//-->to print the space count
		// s-space.;
		// Pattern patternObj = Pattern.compile("\\S");//-->to print the skipping space
		// in password S-skipping space.;
		// Pattern patternObj = Pattern.compile("\\d");// -->to print the digits in
		// password d-digits printing.;
//		Pattern patternObj = Pattern.compile("\\D");// -->to print the digits in password D-digits skipping printing and
//													// all others..;
//		Matcher matcherObj = patternObj.matcher(password);
//		int count = 0;
//		while (matcherObj.find()) {
//			// count++;
//			System.out.print(matcherObj.group());
//		}
//		System.out.println(count);

		
		
		
//		 String mobile = "6383774571";
//		 // Pattern patternObj = Pattern.compile("[6-9][0-9]{9}");//-->to print the valid mobile numbers.
//		  Pattern patternObj = Pattern.compile("(0|91)?[0-9]{10}");//-->to print the valid mobile numbers.  
//		  Matcher matcherObj = patternObj.matcher(mobile);
//		    while(matcherObj.find())
//		    {
//		      System.out.print(matcherObj.group());
//		    }
		
		
		
		String pattern = "-";
	    String input = "28-March-2023";
	    Pattern patternObj = Pattern.compile(pattern);
	    String[] items = patternObj.split(input);
	    for(int i=0;i<items.length;i++)
	    {
	      System.out.println(items[i]);
	    }
	}	
}