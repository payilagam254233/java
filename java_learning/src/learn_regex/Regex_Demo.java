package learn_regex;

public class Regex_Demo {
	String name1;
	String name2;

	public Regex_Demo(String name1, String name2) {
		this.name1 = name1;
		this.name2 = name2;
	}

	public Regex_Demo() {
		//also main method
	}

	public static void main(String[] args) {
		Regex_Demo rd = new Regex_Demo();
		Regex_Demo rd1 = new Regex_Demo("yogeshwaran", "santhosh");
		rd.display(rd1);

	}

	private void display(Regex_Demo re) {
		System.out.println(re.name1);
		System.out.println(re.name2);
	}
}