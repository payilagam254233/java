package leranLamda;

import java.util.function.Function;
public class Lambda_Practice {

	public static void main(String[] args) {
		Functional_interface_practice fip = (no1, no2) -> {
			return (no1 / no2);
		};
		System.out.println(fip.add(700, 5));
		Functional_interface_practice.divide();
		Functional_interface_practice.test();
		
		Function<Float,Float> f=(i)->{return i;};
		System.out.println(f.apply((float) 555555));
		Function<Byte,Byte> g=(i)->{return i;};
		System.out.println(g.apply((byte) 45));
		Function<String,String> h=(i)->{return i;};
		System.out.println(h.apply((String)"yogesh"));

	}

}
