package leranLamda;

@FunctionalInterface

public interface Functional_interface_practice {
	public static void main(String[] args) {
		Functional_interface_practice.divide();
		Functional_interface_practice.test();

	}

	abstract int add(int no1, int no2);

	public static void test() {
		double a = 500.5;
		double b = 20.888;
		double c = a * b;
		System.out.println(c);

	}

	static void divide() {
		int a = 500;
		int b = 20;
		int c = a / b;
		System.out.println(c);

	}

}
