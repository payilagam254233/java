package leranLamda;

import java.util.function.Function;
import java.util.function.Predicate;

public class Lamda_demo {
	public static void main(String[] args) {
//		FunactionalInterfaceDemo fid = (no1,no2)->{return no1+no2;};
//		System.out.println(fid);
//		System.out.println(fid.add(10,20));
//		FunactionalInterfaceDemo.display();
//		fid.test();
		// int = integer
		// float = Float
		// byte = Byte
		// double = Double

		Function<Double, Double> f = (i) -> {
			return i;
		};
		System.out.println(f.apply(50.5));

		Predicate<Integer> h = (i) -> {
			if (i <= 100)
				return true;
			return false;
		};
		System.out.println(h.test(99));
	}
}