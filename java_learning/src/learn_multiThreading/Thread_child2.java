package learn_multiThreading;

public class Thread_child2 extends Thread{

	public void run() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.out.println("output interrupted by seconds");
		}
		for (int no = 1; no <= 5; no++) {
			System.out.println(no);
		}
	}
}