package learn_multiThreading;

public class Thread_Demo {
	public static void main(String[] args) {
		Thread_child tc = new Thread_child();
		tc.start();
		System.out.println("tc " + tc.getName());
		System.out.println("tc priority "+tc.getPriority());
		System.out.println("tc id " + tc.getId());
		System.out.println("tc "+tc.isDaemon());
		System.out.println("tc "+tc.getState());

		Thread_child tc1 = new Thread_child();
		tc1.start();
		System.out.println("tc1 priority "+tc1.getPriority());
		System.out.println("tc1 " + tc1.getName());
		System.out.println("tc1 id " + tc1.getId());
		System.out.println("tc1 "+tc1.isDaemon());
		System.out.println("tc1 "+tc1.getState());
		for (int i = 1; i <= 5; i++) {
			System.out.println("Thread_Demo" + i);
		}
	}
}