package learnException;

import java.util.Scanner;

public class Exception_practice {

	public static void main(String[] args) {
		Exception_practice ep = new Exception_practice();
		Scanner sd = new Scanner(System.in);
		System.out.println("Enter 2 numbers :");
		int no1 = sd.nextInt();
		int no2 = sd.nextInt();

		ep.division(no1, no2);
		ep.Subtract(no1, no2);

	}

	private void Subtract(int no1, int no2) {
		System.out.println("subraction=>" + (no1 - no2));

	}

	private void division(int no1, int no2) {
		try {
			int[][] ar = new int[no1][no2];
			System.out.println("Array length=>" + ar.length);
			System.out.println("division=>" + (no1 / no2));
		} catch (ArithmeticException ae) {
			System.out.println("check your no2");
		}
		catch(NegativeArraySizeException na) {
			System.out.println("Check array value");
			
		}
	}

}
