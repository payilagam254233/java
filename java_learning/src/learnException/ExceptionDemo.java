package learnException;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		ExceptionDemo ed = new ExceptionDemo();
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter 2 numbers :");
		int no1 = sc.nextInt();
		int no2 = sc.nextInt();

		ed.divide(no1, no2);
		ed.add(no1, no2);
	}

	private void add(int no1, int no2) {
		System.out.println("add=>" + (no1 + no2));
	}

	private void divide(int no1, int no2) {
		try {
			//error possible area-block
			System.out.println("divide=>" + (no1 / no2));
			int[] ar = new int[no1];
			System.out.println("length=>" + ar.length);
			for (int i = 0; i < 10; i++) {
				System.out.println(ar[i]);
			}

		} catch (ArithmeticException ae) {
			System.out.println("divide kooda panna therila ma**re");
		} catch (NegativeArraySizeException na) {
			System.out.println("check array value");
		} catch (ArrayIndexOutOfBoundsException ai) {
			System.out.println("out of bound");
		} catch (Exception e) {
			//error handling area-block
			System.out.println("something wrong");
		} finally {
			//code cleaning area-block
			System.out.println("finally");
		}
//		catch(Throwable e) {
//			System.out.println("something wrong");
//		} //parent class of Exception
	}

}