package learnException;

public class Exam {

	public static void main(String[] args) {
		Low_Attendnce_Exception lae = new Low_Attendnce_Exception();
		lae.allow(80);
		int[] marks = { 98, 66, 77, 88 };
		try {
			ThrowsDemo.calculate(marks);
		} catch (NegativeArraySizeException ar) {
			System.out.println("check array value");
		} catch (ArrayIndexOutOfBoundsException ar) {
			System.out.println(ar.getMessage());
//			ar.printStackTrace();//to print the exception name.
		} catch (Exception e) {

		}
	}
}