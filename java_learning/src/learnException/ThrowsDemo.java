package learnException;

public class ThrowsDemo {

	public static void main(String[] args) {
		int[] marks = { 89, 99, 01, 26, 11, 11 };
		try {
			calculate(marks);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void calculate(int[] marks)
			throws ArrayIndexOutOfBoundsException, NegativeArraySizeException, Exception {
		for (int i = 0; i <=5; i++) {
			System.out.println(marks[i]);

		}
	}
}