package learn_scanner;

import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) {
//		Scanner sc = new Scanner(System.in);
//		System.out.println("Enter your name:");
//		String name=sc.next(); //it reads before space.
//		String name = sc.nextLine(); // it reads full name with space.
//		System.out.println("Enter your age:");
//		int age = sc.nextInt();
//		System.out.println("Enter your height:");
//		double height = sc.nextDouble();
//		System.out.println("Enter your mobile number: ");
//		long mobile = sc.nextLong();
//		System.out.println("name: " + name);
//		System.out.println("age: " + age);
//		System.out.println("height: " + height);
//		System.out.println("number: " + mobile);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter your number:");
		int no=sc.nextInt();
		String [] name=new String[no];
		System.out.println("your array length is: "+name.length);
		for(int i = 0;i<name.length;i++){
			System.out.println("Enter name :");
			name[i]=sc.next();
		}
		for(int i=0;i<name.length;i++){
			System.out.print(name[i]+" ");
		}
	}

}
