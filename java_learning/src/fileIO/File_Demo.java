package fileIO;

import java.io.*;

public class File_Demo {

	public static void main(String[] args) throws IOException {
		File_Demo fd = new File_Demo();
		// fd.magic();
		// fd.buffer();
		// fd.day1();
		// fd.day2();
		fd.stream();
	}

	private void stream() throws IOException {
		File input = new File("/home/yogi/Downloads/wallpaper.png");
		File output = new File("/home/yogi/Desktop/wallpaper.png");
		FileInputStream reader = new FileInputStream(input);
		FileOutputStream writer = new FileOutputStream(output);

		int i = reader.read();
		while (i != -1) {
			writer.write(i);
			i = reader.read();
		}

	}

	private void day2() {
		File note = new File("/home/yogi/Videos/Biriyanishop1/chickenbiriyani/Mock1.txt");
		try {
			FileReader reader = new FileReader(note);
			BufferedReader br = new BufferedReader(reader);
			String line = br.readLine();
			while (line != null) {
				System.out.println(line);
				line = br.readLine();
			}
			/*
			 * int i = reader.read(); while(i!=-1) { System.out.print((char)i);
			 * i=reader.read(); }
			 */
		} catch (FileNotFoundException io) {
			io.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}

	}

	private void day1() {
		// File note = new File("/home/yogi/Documents/Mock1.txt");
		/*
		 * String[] file_folder =note.list(); for(int i = 0;i<file_folder.length;i++) {
		 * if(file_folder[i].endsWith(".java")) { System.out.println(file_folder[i]); }
		 * }
		 */ // -->listing method by ends with and starts with.

		/*
		 * File[] ff = note.listFiles(); for(int i=0;i<ff.length;i++) {
		 * if(ff[i].isDirectory()) {
		 * 
		 * System.out.println(ff[i].getName()); } }
		 */
		File note = new File("/home/yogi/Videos/Biriyanishop1/chickenbiriyani/MenuItems.txt");

	}

	private void buffer() {
		File note = new File("/home/yogi/Videos/Biriyanishop1/chickenbiriyani/MenuItems.txt");

		try {
			FileWriter pen = new FileWriter(note, false);
			BufferedWriter bw = new BufferedWriter(pen);
			bw.append("vijay");
			bw.newLine();
			bw.append("ajith");
			bw.newLine();
			bw.append("rajini");
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void magic() {
		// File note = new
		// File("/home/yogi/Videos/Biriyanishop1/chickenbiriyani/MenuItems.txt");

//		System.out.println(note.mkdir());//-->to make directory.
//		System.out.println(note.mkdirs());//-->to make directories.
//		System.out.println(note.canExecute());
//		System.out.println(note.canRead());
//		System.out.println(note.canWrite());
//		System.out.println(note.delete());
//		System.out.println(note.exists());

//		try {
//			note.createNewFile();
//			FileWriter pen = new FileWriter(note,true);
//			pen.append("\nidly");
//			pen.append("\npongal");
//			pen.close();
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		}		
	}

}
