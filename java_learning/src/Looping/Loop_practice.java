package Looping;

public class Loop_practice {

	public static void main(String[] args) {
		Loop_practice lp = new Loop_practice();
		lp.loop1();
		System.out.println();
		lp.loop2();
		System.out.println();
		lp.loop3();
		System.out.println();
		lp.loop4();
		System.out.println();
		lp.loop5();
		System.out.println();
		lp.loop6();
		System.out.println();
		lp.loop7();
		lp.loop8();
		int num=5;
		System.out.println(++num);
		System.out.println(num--);
		System.out.println(num);
byte no = 127;
byte no1= 2;
int no2 = no+no1;
System.out.println(no2);

	}

	private void loop8() {
		for (int i = 0; i < 10; i++) {
			// System.out.println(i);

			if (i == 5) {
				continue;
				// break;
			}
			System.out.println(i);

		}

	}

	private void loop7() {
		for (int row = 0; row <= 5; row++) {
			for (int col = 1; col <= 5; col++) {
				System.out.print(row + " ");
			}
			System.out.println();
		}

	}

	private void loop6() {
		int no = 1;
		while (no <= 5) {
			System.out.print(no + " ");
			no++;
		}
	}

	private void loop5() {
		int hell = 15;
		while (hell >= 0) {
			System.out.print(hell + " ");
			hell = hell - 3;
		}

	}

	private void loop4() {
		int start = 5;
		while (start > -2) {
			System.out.print(start + " ");
			start = start - 1;
		}
	}

	private void loop3() {
		int count = 0;
		while (count < 5) {
			System.out.print("hii" + " ");
			count = count + 1;
		}
	}

	private void loop2() {
		int task = 0;
		while (task < 5) {
			System.out.println("hii" + " ");
			task = task + 1;
		}
	}

	private void loop1() {
		int no = 1;
		do {
			System.out.print(no + " ");
			no++;
		} while (no <= 5);
	}

}
