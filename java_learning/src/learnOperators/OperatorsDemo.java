package learnOperators;

public class OperatorsDemo {

	public static void main(String[] args) {
		OperatorsDemo od = new OperatorsDemo();
		od.operator();
	}

	private void operator() {
		int no = 10;
		// post unary operator
		System.out.println(no++);// 10->11
		System.out.println(no); // 11
		// no=11
		// post unary operator
		System.out.println(no--); // 11->10
		System.out.println(no);// 10
		// pre unary operator
		// no=10
		System.out.println(++no);// 10->11

		System.out.println(no);// 11

		System.out.println(--no);// 11->10

		System.out.println(no);// 10

		System.out.println(3 - 2 + 2 * 2 + 3);// 3-2+4+3
												// 3-2+7
												// 1+7=8
		System.out.println(4 / 2 + 1 - 4 * 2 + 10);// 2+1-8+10
													// 3-8+10
													// -5+10=5

		int no1 = 10;
		int no2 = 15;
		System.out.println(no1 > no2 || ++no2 < no1); // or gate
		System.out.println(no2);
		// no=16
		System.out.println(no1 < no2 && --no2 < no1); // and gate
		System.out.println(no2);

		System.out.println(5 + 2 - (2 + 2) * 3);

	}

}
