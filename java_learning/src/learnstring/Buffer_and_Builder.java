package learnstring;

public class Buffer_and_Builder {

	public static void main(String[] args) {
		String[] names = { "yogeshwaran", "manohar", "devaraju" };
		Buffer_and_Builder bb = new Buffer_and_Builder();
		bb.joinName(names);

	}

	private void joinName(String[] names) {
		String join = "";
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < names.length; i++) {
			join = join + names[i];
			sb.append(names[i]);
			System.out.println("StringBuffer---->" + sb.hashCode());
			System.out.println("String----->" + join.hashCode());
		}
		System.out.println(join);
		System.out.println(sb);
		String a = "nageswari";
		a = a.toUpperCase();
		System.out.println(a);

	}

}
