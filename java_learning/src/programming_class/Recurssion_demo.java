package programming_class;

public class Recurssion_demo {

	public static void main(String[] args) {
		Recurssion_demo rd=new Recurssion_demo();
		rd.display(1);
		System.out.println(rd.factorial_recursive(5));
		

	}

	private int factorial_recursive(int no) {
		if(no==1) {
			return no;
		}
		else {
		return no*factorial_recursive(no-1);
		}
		
	}

	private void display(int no) {
		System.out.print(no+" ");
		no++;
		if(no<=5)
		display(no);
		System.out.println();
	}

}
