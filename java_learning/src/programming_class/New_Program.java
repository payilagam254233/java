package programming_class;

public class New_Program {

	public static void main(String[] args) {
		New_Program np= new New_Program();
		np.pattern_1();
		np.pattern_2();
		np.pattern_3();
		np.pattern_4();
		np.pattern_5();
		np.pattern_6();
		np.pattern_7();
		np.pattern_8();
		np.pattern_9();
		np.pattern_10();
		np.pattern_11();
		np.pattern_12();
		np.pattern_13();
		np.pattern_14();
		np.pattern_15();
		np.pattern_16();
		np.pattern_17();
		

	}

	private void pattern_17() {
		
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=row;col++) {
				if(row==col||(row+col)%2==0) {
			System.out.print(0+" ");
				}
				else {
					System.out.print(1+" ");
				}
			}
			
			System.out.println();
		}
		
	}

	private void pattern_16() {
		
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=row;col++) {
				if(col%2==0) {
			System.out.print(1+" ");
				}
				else {
					System.out.print(0+" ");
				}
			}
			
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_15() {
		int no=1;
		for(int row=1;row<=4;row++) {
			for(int space=1;space<=4-row;space++) {
				System.out.print("  ");
			}
			for(int col=1;col<=no;col++) {
			System.out.print(1+" ");
			}
			no=no+2;
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_14() {
		
		for(int row=1;row<=7;row+=2) {
			for(int col=1;col<=row;col++) {
			System.out.print("* ");
			}
			
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_13() {
		int no=1;
		for(int row=1;row<=4;row++) {
			for(int col=1;col<=no;col++) {
			System.out.print("* ");
			}
			no=no+2;
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_12() {
		for(int row =5;row>=1;row--) {
			for(int col =1;col<=row;col++) {
				System.out.print("  ");
				
				
			}
			for(int col=1;col<=6-row;col++) {
				System.out.print(col+" ");
				
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_11() {
		int end=1;
		for(int row =1;row<=5;row++) {
			for(int col=row;col<=end;col++) {
				System.out.print((row+col)+" ");
			}
			System.out.println();
			end=end+2;
		}
		System.out.println();
	}

	private void pattern_10() {
		int end=1;
		for(int row =1;row<=5;row++) {
			for(int col=row;col<=end;col++) {
				System.out.print(col+" ");
			}
			System.out.println();
			end=end+2;
		}
		System.out.println();
	}

	private void pattern_9() {
		int no=1;
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=row;col++) {
				System.out.print(no+" ");
				no++;
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_8() {
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=row;col++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		System.out.println();
	}



	private void pattern_7() {
		char row='A';
		while(row<='E') {
			
		for(char col='A';col<=row;col++) {
			System.out.print(col+" ");
		}
		System.out.println(); row++;

		}
	System.out.println();	
	}


	private void pattern_6() {
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=row;col++) {
				System.out.print(col+" ");
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_5() {
		for(int row=5;row>=1;row--) {
			for(int col=5;col>=6-row;col--) {
				System.out.print(col+" ");
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_4() {
		for(int row=1;row<=5;row++) {
			for(int col=5;col>=6-row;col--) {
				System.out.print(col+" ");
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_3() {
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=6-row;col++) {
				System.out.print(col+" ");
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_2() {
		int row=1;
		while(row<=5) {
		for(int col=1;col<=row;col++) {
			System.out.print(col+" ");
		}
		System.out.println(); row++;

		}
		System.out.println();
	}

	private void pattern_1() {
		for(int row=5;row>=1;row--) {
			for(int col=1;col<=row;col++) {
				System.out.print(col+" ");
			}
			System.out.println();
		}
		System.out.println();
	}

}
