package programming_class;

public class Array_test_Practice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Array_test_Practice atp = new Array_test_Practice();
		//atp.arrayConcat();
		//atp.arrayMutliply();
		//atp.palienrome();
		//atp.sumoftwonumbers();
		//atp.contain();
		//atp.minusculetough();
		atp.numberpattern();

	}

	private void numberpattern() {
		// TODO Auto-generated method stub
		 int n = 3; // Size of the matrix

	        for (int i = 0; i < n; i++) {
	            for (int j = 1; j <= n; j++) {
	                int value = 0;
	                

	                if (i % 2 == 0) {
	                    // For even rows, print increasing order
	                    value = i * n + j;
	                } else {
	                    // For odd rows, print decreasing order
	                    value = (i + 1) * n - j + 1;
	                }

	                System.out.print(value + " ");
	            }
	            System.out.println(); // Move to the next line after each row
	        }   
	    }
	

	private void minusculetough() {
		// TODO Auto-generated method stub
		String input = "xyz";
		int key = 2;
		String output = "";
		for (int i = 0; i < input.length(); i++) {
			char currentchar = input.charAt(i);

			char ch =  (char) (currentchar + key);

			if (Character.isLowerCase(currentchar) && ch > 'z') {
				ch = (char) ('a' + (ch - 'z' - 1));
			}

			if (Character.isUpperCase(currentchar) && ch > 'Z') {
				ch = (char) ('A' + (ch - 'Z' - 1));
			}

			output = output + ch;
		}
		System.out.println(output);

	}

	private void contain() {
		// TODO Auto-generated method stub
//		array1=[5,6,11,1,-1,-4]
//		array2=[-1,-4,6]
//		comparing two arrays and subsequence array contains value means true.

		int[] arr1 = { 5, 6, 11, 1, -1, -4 };
		int[] arr2 = { -1, -4, 6 };
		int count = 0;
		for (int i = 0; i < arr2.length; i++) {
			for (int j = 0; j < arr1.length; j++) {
				if (arr2[i] == arr1[j]) {
					count++;
					break;
				}
			}
		}
		if (count == arr2.length) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}

	}

	private void sumoftwonumbers() {
		// TODO Auto-generated method stub
//		input=[1,2,11,10,-1]
//		sum of two numbers in this array is 10 
//		so output is 11-1=10 in this given array.

		int[] arr = { 1, 2, 11, 10, -1 };
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] + arr[j] == 10) {
					System.out.println(arr[i] + arr[j]);
					break;
				}
			}
		}
	}

	private void palienrome() {
		// TODO Auto-generated method stub
		// input="abcdcba";
		// output="abcdcba"; (reversed)
		// result=true
		String input = "abcdcba";
		String empty = "";
		for (int i = input.length() - 1; i >= 0; i--) {
			empty = empty + input.charAt(i);
		}
		System.out.println();
		if (empty.equals(input)) {
			System.out.println("Its palinrome : " + empty);
		} else {
			System.out.println("Its not palinrome : " + empty);
		}

	}

	private void arrayMutliply() {
		// TODO Auto-generated method stub
		// [1,2,3] input1
		// [4,5,6,7] input2
		// [4,10,18,7] output
		int[] a = { 1, 2, 3 };
		int[] b = { 4, 5, 6, 7 };
		int len1 = a.length;
		int len2 = b.length;
		int small;
		int big;
		if (len1 < len2) {
			small = len1;
			big = len2;
		} else {
			small = len2;
			big = len1;
		}
		int[] c = new int[big];
		for (int i = 0; i < small; i++) {

			c[i] = a[i] * b[i];
			c[3] = b[3];
		}
		for (int i = 0; i < c.length; i++) {
			System.out.print(c[i] + " ");
		}
	}

	private void arrayConcat() {
		// TODO Auto-generated method stub
		// [10,20,30]input 1
		// [40,50]input 2
		// output[10,20,30,40,50]
		int[] a = { 10, 20, 30 };
		int[] b = { 40, 50 };
		int len = a.length + b.length;
		int[] c = new int[len];
		for (int i = 0; i < a.length; i++) {
			c[i] = a[i];

		}
		int j = 0;
		for (int i = a.length; i < c.length; i++) {
			c[i] = b[j];
			j++;

		}
		for (int i = 0; i < c.length; i++) {
			System.out.print(c[i] + " ");
		}
		System.out.println();
	}

}
