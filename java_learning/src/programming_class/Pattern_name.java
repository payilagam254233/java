package programming_class;

public class Pattern_name {

	public static void main(String[] args) {
		Pattern_name pn = new Pattern_name();
		pn.pattern_Y();
		pn.pattern_O();
		pn.pattern_G();
		pn.pattern_E();
		pn.pattern_S();
		pn.pattern_H();
		pn.pattern_W();
		pn.pattern_A();
		pn.pattern_R();
		pn.pattern_A_2();
		pn.pattern_N();

	}

	private void pattern_N() {
		for(int row=1;row<=7;row++) {
		for (int col = 1; col <= 7; col++) {
			if(col==1||row==col||col==7) {
			System.out.print("* ");
			}
			else {
				System.out.print("  ");
			}
		}
		System.out.println();
		}
	}

	private void pattern_A_2() {
		pattern_A();
		
	}

	private void pattern_R() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col == 7) {
					if (row == 1 && col == 7 || row == 7 && col == 7) {
						System.out.print("  ");
					}

					else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}

			}

			System.out.println();
		}
		for(int col=1;col<=7;col++) {
			for(int row=1;row<=7;row++) {
				if(row==1||col==row) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pattern_A() {

		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if ((col == 1 && row >= 4) || (col == 7 && row >= 4) || row == 4 || (row + col == 5)
						|| col - row == 3) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_W() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || col == 7 || (row == col && row >= 4) || (row + col == 8 && row >= 4)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();

	}

	private void pattern_H() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || row == 4 || col == 7) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_S() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if ((row == 1 && col != 1) || (row == 4 && (col != 1 && col != 7)) || row == 7 && (col != 7)
						|| (col == 1 && (row == 2 || row == 3)) || (col == 7 && (row == 5 || row == 6))) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_E() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || col == 1 || row == 7 || row == 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_G() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || col == 1 || row == 7 || (col == 7 && row > 4) || (row == 4 && col > 4)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_O() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || col == 7 || row == 7 || col == 1) {
					if ((col == 1 && row == 1) || (row == 1 && col == 7) || (row == 7 && col == 7)
							|| (row == 7 && col == 1)) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_Y() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if ((col == row && col <= 4) || (col + row == 8 && row <= 4) || (col == 4 && row >= 4)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

}
