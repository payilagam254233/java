package programming_class;

public class Array_program {

	public static void main(String[] args) {
		Array_program ap = new Array_program();
		// ap.array_1_max();
		// ap.array_2();
		ap.array_3();// concatination of two arrays
		ap.array_4_add();//adding array output 10 and printing those elements.
		ap.array_5_multiply();//multiplying array output 10 and printing those elements.
		ap.array_6_start_end();//printing adjacent number of arrays.
		ap.non_repeated_array_char();//printing non repeated character in char array.
		//find count of each character?
		int[] a = { 10, 20, 30, 40 };
		a = ap.array_3_shiftleft(a);
		for (int j = 0; j < a.length; j++) {
			System.out.print(a[j] + " ");
		}

	}

	private void non_repeated_array_char() {
		int j;
		char [] name = {'y','o','g','e','s','h','w','a','r','a','n'};
		for(j=0;j<name.length;j++) {
			char ch=name[j]; boolean present = false;
			for(int i=j+1;i<name.length;i++) {
				if(ch!=name[i]) {
					continue;
				}
				else {
					System.out.println(ch+" is first repeated character");
					present = true;
					break;
				}
			}
			if(present==true) {
				break;
				}
			}
		if(j==name.length) {
			System.out.println("there is no non-repeated letters");
		}
		}
		
	

	private void array_6_start_end() {
		int [] a = {1,5,13,16,21,27};
		for(int i = 0;i<a.length-1;i++) {
			int start = a[i];
			int end=a[i+1];
			for(int no=start;no<=end;no++) {
				System.out.print(no+" ");
			}
			
		}
		System.out.println();
	}

	private void array_5_multiply() {
		// TODO Auto-generated method stub
		int[] a = { 1, 8, 2, 7, 3, 9 };
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] * a[j] >= 10 && a[i] * a[j] <= 99) {
					System.out.println(a[i] + " " + a[j]);
				}
//			System.out.print(a[i]+" ");
			}
		}
		System.out.println();
	}

	private void array_4_add() {
		int[] a = { 1, 8, 2, 7, 3, 9 };
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] + a[j] == 10) {
					System.out.println(a[i] + " " + a[j]);
				}
//			System.out.print(a[i]+" ");
			}
		}
		System.out.println();
	}

	private void array_3() {
		int[] a = { 10, 20, 30, 35 };
		int[] b = { 40, 50, 60 };
		int[] c = new int[a.length + b.length];
		int j = 0;
		for (int i = 0; i < c.length; i++) {
			if (i < a.length) {
				c[i] = a[i];
			} else {
				c[i] = b[j];
				j++;
			}
			System.out.print(c[i] + " ");

		}
		System.out.println();
	}

	private int[] array_3_shiftleft(int[] a) {

		int temp = a[0];
		int i = 0;
		while (i < a.length - 1) {
			a[i] = a[i + 1];
			i++;

		}
		a[i] = temp;
		return a;
	}

	private void array_2() {
		int[] a = { 1000, 4, 67, 98, 999, 99 };
		int big = Integer.MAX_VALUE;
		int sbig = Integer.MAX_VALUE;
		for (int i = 0; i < a.length; i++) {
			if (a[i] < big) {
				sbig = big;
				big = a[i];
			} else if (a[i] < big) {
				sbig = a[i];
			}
		}

		System.out.println(big + " " + sbig);

	}

	private void array_1_max() {

		int[] ar = { 150, -100, 129, 500, -1000 };

		for (int i = 1; i < ar.length; i++) {
			if (ar[i] < 0) {
				System.out.print(ar[i]);

			}

		}
		System.out.println();
	}

}
