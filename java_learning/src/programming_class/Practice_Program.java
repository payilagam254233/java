package programming_class;

public class Practice_Program {

	public static void main(String[] args) {
		Practice_Program p = new Practice_Program();
		p.program1();
		p.program2();
		p.SquareOfNumbers();
		p.CubeOfNumbers();
		p.SumOfNumbers();
		p.MultiplicationOfNumbers();
		p.factorial();
		p.common();
		p.series();
		p.Series1();
		p.Squareroot();
		p.divisors();
		p.GreatestCommonDivisor();
		p.LeastCommonDivisor();
		p.SumOfDigits();
		p.ReverseOfNumber();
		p.CountOfDigits();
		p.palindrome();
		p.primeNumber();
	}

	private void primeNumber() {
		//Prime number
		int no=3;int no1=3;
		if(no%no1==0) {
			System.out.println("Its a Prime Number");
		}
		else {
			System.out.println("Its not a Prime Number");
		}
		
	}

	private void palindrome() {
		//121=121(reversed)
		int no=121;int rev=0;int sum=0;int count=0;int no2=no;
		while(no>0) {
			int rem=no%10;
			sum=sum+rem;
			rev=(rev*10)+rem;
			no=no/10;
			count++;
		}
		System.out.println(rev);
		if(rev==no2) {
			System.out.println("It is palindrome");
		}
		else {
			System.out.println("It is not a plaindrome");
		}
	}

	private void CountOfDigits() {
		//1234=count=4
		int no=1234,sum=0,count=0;
		while(no>0) {
			int rem=no%10;
			no=no/10;
			sum=sum+rem;
			count++;
		}
		System.out.println("count of given number : "+count);
		
	}

	private void ReverseOfNumber() {
		//4563=3654
		int no=4563;
		int rev=0,count=0,sum=0;
		while(no>0) {
			int rem=no%10;
			sum=sum+rem;
			rev=(rev*10)+rem;
			no=no/10;
			count++;
		}
		System.out.println(rev);
	}

	private void SumOfDigits() {
		// 9 + 7 + 6 + 3=25
		int no = 9763;
		int sum = 0;
		while (no > 0) {
			int rem = no % 10;
			sum = sum + rem;
			no = no / 10;
		}
		System.out.println(sum);
	}

	private void LeastCommonDivisor() {
		// Least Common Divisor Of Given Numbers
		int no1 = 3;
		int no2 = 9;
		int big = no1 > no2 ? no1 : no2;
		if (big % no1 == 0 && big % no2 == 0) {
			System.out.println("big : " + big);
		}

		int no3 = 7;
		int no4 = 9;
		int big1 = no3 > no4 ? no3 : no4;
		while (true) {
			if (big1 % no3 == 0 && big1 % no4 == 0) {
				System.out.println("big1 : " + big1);
				break;
			}
			big1++;
		}

	}

	private void GreatestCommonDivisor() {
		// Greatest Common Divisor Of Given Numbers
		int no = 100;
		int no1 = 60;
		int div = 2;
		while (div <= no1) {
			if (no % div == 0 && no1 % div == 0) {
				System.out.print(div + " ");
			}
			div++;
		}
		System.out.println();
	}

	private void divisors() {
		// Divisors of number
		int no = 80;
		int div = 2;
		while (div <= no) {
			if (no % div == 0) {
				System.out.print(div + " ");

			}
			div++;
		}
		System.out.println();

	}

	private void Squareroot() {
		// 49=7
		int no = 49;
		int no1 = 1;
		while (no1 <= no / 2) {
			if (no / no1 == no1) {
				System.out.println("Square root of 49 is : " + no1);
				break;
			}
			no1++;
		}

	}

	private void Series1() {
		// 1 11 121
		int no = 1;
		while (no <= 121) {
			System.out.print(no + " ");
			no = no * 11;
		}
		System.out.println();
	}

	private void series() {
		// 1 11 111 1111
		int no1 = 1;
		int no2 = 10;
		while (no1 <= 1111) {
			System.out.print(no1 + " ");
			no1 = no1 + no2;
			no2 = no2 * 10;
		}
		System.out.println();
	}

	private void common() {
		// 1x10 2x9 3x8 4x7 5x6
		int no1 = 1;
		int no2 = 10;
		while (no1 <= 5) {
			System.out.println(no1 + " X " + no2 + " = " + no1 * no2 + " ");
			no1++;
			no2--;
		}

	}

	private void factorial() {
		// 5*4*3*2*1=120(5!)
		int fact = 5;
		int no = 1;
		while (fact > 0) {
			no = fact * no;
			fact--;
		}
		System.out.println("Factrial : " + no);

	}

	private void MultiplicationOfNumbers() {
		// 1*2*3*4*5=120
		int no1 = 1;
		int no2 = 1;
		while (no1 <= 5) {
			no2 = no1 * no2;
			no1++;
		}
		System.out.println("Mulitply : " + no2);

	}

	private void SumOfNumbers() {
		// 1+2+3+4+5=15
		int sum = 0;
		int no = 1;
		while (no <= 5) {
			sum = sum + no;
			no++;
		}
		System.out.println("Add : " + sum);

	}

	private void CubeOfNumbers() {
		// 1 8 27 64 125
		int no1 = 1, no2 = 2;
		while (no1 <= 125) {
			System.out.print(no1 + " ");
			no1 = no2 * no2 * no2;
			no2++;
		}
		System.out.println();

	}

	private void SquareOfNumbers() {
		// 1 4 9 16 25 36
		int no = 1;
		int no2 = 2;
		while (no <= 36) {
			System.out.print(no + " ");
			no = no2 * no2;
			no2++;
		}
		System.out.println();
	}

	private void program2() {
		// 3 5 8 13 22
		int no1 = 3;
		int no2 = 1;
		while (no1 <= 22) {
			System.out.print(no1 + " ");
			no1 = (no1 * 2) - no2;
			no2 = no2 + 1;

		}
		System.out.println();

	}

	private void program1() {
		// 1 3 6 10 15 21 28
		int no1 = 1, no2 = 2;
		while (no1 <= 28) {
			System.out.print(no1 + " ");
			no1 = no1 + no2;
			no2++;
		}
		System.out.println();
	}

}
