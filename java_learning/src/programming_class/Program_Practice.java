package programming_class;

import java.util.Scanner;

public class Program_Practice {

	public static void main(String[] args) {
		Program_Practice pp = new Program_Practice();
//		pp.MultiplyofFirstFiveNumbers();
//		pp.AdditionOfFirstFiveNumbers();
//		pp.factorial5();
//		pp.stringreverse();
//		pp.striptrailing();
//		pp.striptrailingandleading();
//		pp.stripleading();
//		pp.extraspacremoval();
//		pp.fibanocciseries();
//		pp.firstandlastchartouppercase();
//		pp.maxnumber();
//		pp.minnumber();
//		pp.uppercase();
//		pp.lowercase();
//		pp.divisor();
//		pp.trim();
//		pp.touppercase();
//		pp.tolowercase();
		
		
		
		//fibanocci series 
		int a=0,b=1;
	    while(a<=34) {
	      System.out.print(a+" ");
	      b=a+b;
	      a=b-a;
	    }

		// string reverse program.....
		/*
		 * String name = "yogeshwaran"; String Rname = ""; for(int
		 * t=name.length()-1;t>=0;t--) { Rname = Rname+name.charAt(t); }
		 * System.out.println("Reversed Name : "+Rname);
		 */

	}

	private void tolowercase() {
		String input = "YOGESHWARAN";
		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			ch = (char) (ch + 32);
			System.out.print(ch);

		}

	}

	private void touppercase() {
		String input = "yogeshwaran";
		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			ch = (char) (ch - 32);
			System.out.print(ch);

		}
System.out.println();
	}

	private void trim() {
		String input = "    Hello World    ";
		String Trimmed = input.trim();
		System.out.println("input : " + input);
		System.out.println("Trimmed : " + Trimmed);

	}

	private void divisor() {
		int no = 80;
		System.out.println("Divisors of given number are : ");
		for (int i = 1; i <= no; i++) {
			if (no % i == 0) {
				System.out.print(i + " ");
			}
		}
	}

	private void uppercase() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your Name Smaller Letters : ");
		String name = sc.nextLine();
		System.out.print(name.toUpperCase());
		System.out.println();
	}

	private void lowercase() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your Name Capital Letters : ");
		String name = sc.nextLine();
		System.out.print(name.toLowerCase());
		System.out.println();

	}

	private void minnumber() {
		int[] numbers = { 5, 8, 2, 10, 7, 1 };

		int minNumber = numbers[0];

		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] < minNumber) {
				minNumber = numbers[i];
			}
		}

		System.out.println("The minimum number is: " + minNumber);
	}

	private void maxnumber() {
		int[] numbers = { 5, 8, 2, 10, 7, 100 };

		int maxNumber = numbers[0];

		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] > maxNumber) {
				maxNumber = numbers[i];
			}
		}

		System.out.println("The maximum number is: " + maxNumber);
	}

	private void firstandlastchartouppercase() {
		String name = "yogeshwaran";

		for (int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if (i == 0 || i == name.length() - 1) {
				ch = (char) (ch - 32);
				System.out.print(ch);
			} else {
				char ch2 = name.charAt(i + 1);
				if (ch2 == ' ') {
					ch = (char) (ch - 32);
					System.out.print(ch);

				}

				else {
					ch2 = name.charAt(i - 1);
					if (ch2 == ' ') {
						ch = (char) (ch - 32);
						System.out.print(ch);

					} else
						System.out.print(ch);
				}
			}
		}

	}

	private void fibanocciseries() {
		int n = 11;
		int firstTerm = 0, secondTerm = 1;

		System.out.println("Fibonacci Series:");

		for (int i = 1; i <= n; i++) {
			System.out.print(firstTerm + " ");

			int nextTerm = firstTerm + secondTerm;
			firstTerm = secondTerm;
			secondTerm = nextTerm;
		}

	}

	private void extraspacremoval() {
		String s = "how     are    you";
		boolean space_present = false;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				System.out.print(s.charAt(i));
				space_present = false;
			} else if (space_present == false) {
				System.out.print(s.charAt(i));
				space_present = true;
			}
		}

	}

	private void stripleading() {
		String s = "   How are you   ";
		int last = 0;
		for (int i = s.length() - 1; i >= 0; i--) {
			if (s.charAt(i) != ' ') {
				last = i;
				break;
			}
		}
		for (int i = 0; i <= last; i++) {
			System.out.print(s.charAt(i));

		}

	}

	private void striptrailingandleading() {
		String s = "   How are you   ";
		int first = 0, last = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				first = i;
				break;
			}
		}
		for (int i = s.length() - 1; i >= 0; i--) {
			if (s.charAt(i) != ' ') {
				last = i;
				break;
			}
		}
		for (int i = first; i <= last; i++) {
			System.out.print(s.charAt(i));

		}

	}

	private void striptrailing() {
		String s = "   How are you   ";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				for (int j = i; j < s.length(); j++) {
					System.out.print(s.charAt(j));
				}
				break;
			}
		}

	}

	private void stringreverse() {
		String original = "Hello";
		String reversed = "";

		for (int i = original.length() - 1; i >= 0; i--) {
			reversed = reversed + original.charAt(i);
		}

		System.out.println("Original: " + original);
		System.out.println("Reversed: " + reversed);
	}

	private void factorial5() {
		int fact = 1;
		int no = 5;
		while (no > 0) {
			fact = fact * no;
			no = no - 1;
		}
		System.out.println("Factorial of 5 : " + fact);

	}

	private void AdditionOfFirstFiveNumbers() {
		int no1 = 1;
		int no2 = 0;

		while (no1 <= 5) {
			no2 = no2 + no1;
			no1++;
		}

		System.out.println("Sum of the first 5 numbers: " + no2);

	}

	private void MultiplyofFirstFiveNumbers() {
		int no1 = 1, no2 = 1;
		while (no1 <= 5) {
			no2 = no1 * no2;
			no1++;

		}
		System.out.print("Multiplication of first five number is : " + no2);
		System.out.println();

	}

}
