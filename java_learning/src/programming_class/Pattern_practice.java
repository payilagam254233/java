package programming_class;

public class Pattern_practice {

	public static void main(String[] args) {
		Pattern_practice pp = new Pattern_practice();
		pp.pattern();
		pp.pattern_1();
		pp.pattern_2();
		pp.pattern_3();
		pp.pattern_4();
		pp.pattern_5();
		pp.pattern_6();
		pp.pattern_7();
		pp.pattern_7_II();
		pp.pattern_8();
		pp.pattern_9();
		pp.pattern_0();

	}

	private void pattern_0() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||col==1||col==7||row==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		
	}

	private void pattern_9() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||col==7||row==4||(col==1&&row<=4)||row==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
		
	}

	private void pattern_8() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||col==7||col==1||row==7||row==4) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_7_II() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||col==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}	
		System.out.println();
	}

	private void pattern_7() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||row+col==8) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_6() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||col==1||row==7||row==4||(col==7&&row>4)) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_5() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||(col==1&&row<=4)||row==4||(col==7&&row>4)||row==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_4() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if((col==1&&row<=4)||row==4||col==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_3() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||col==7||row==4||row==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_2() {
		for(int row = 1;row<=7;row++) {
			for(int col=1;col<=7;col++) {
				if(row==1||(col==7&&row<=4)||row==4||(col==1&&row>3)||row==7) {
				System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
			}
		System.out.println();
	}

	private void pattern_1() {
		for(int row = 1;row<=7;row++) {
		for(int col=1;col<=7;col++) {
			if(col==4) {
			System.out.print("* ");
			}
			else {
				System.out.print("  ");
			}
		}
		System.out.println();
		}
		System.out.println();
	}

	private void pattern() {
		for(int row = 1;row<=7;row++) {
		for(int col=1;col<=7;col++) {
			
			System.out.print("* ");
			
		}
		System.out.println();
		}
		System.out.println();
	}

}
