package programming_class;

public class Pattern_new {

	public static void main(String[] args) {
		Pattern_new pn = new Pattern_new();
		pn.pattern_1();

	}

	private void pattern_1() {
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col < row; col++) {
				System.out.print(" ");

			}
			for(int i=1;i<=6-row;i++) {
				System.out.print("* ");
			}
			
				System.out.println();

		}
	}

}
