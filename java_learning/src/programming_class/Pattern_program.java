package programming_class;

public class Pattern_program {

	public static void main(String[] args) {
		Pattern_program pp = new Pattern_program();
		pp.pattern1();
		pp.pattern2();
		pp.pattern_I();
		pp.pattern_C();
		pp.pattern_O();
		pp.pattern_D();
		pp.pattern_P();
		pp.pattern_B();
		pp.pattern_R();
		pp.pattern_R_2();
		pp.pattern_N();
		pp.pattern_X();
		pp.pattern_Y();
		pp.pattern_Y_2();
		pp.pattern_V();
		pp.pattern_H();
		pp.pattern_A();
		pp.pattern_A_2();
		pp.pattern_M();
		pp.pattern_5();
		pp.pattern_S();
	}

	private void pattern_S() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if ((row == 1 && col != 1) || (row == 4 && (col != 1 && col != 7)) || (row == 7 && col != 7)) {
					System.out.print("* ");
				} else if (col == 1 && (row == 2 || row == 3)) {
					System.out.print("* ");
				} else if (col == 7 && (row == 5 || row == 6)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}

	}

	private void pattern_5() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {

				if (row == 1 || row == 7 || row == 4 || (col == 1 && row <= 4) || (col == 7 && row > 3)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}

			}
			System.out.println();

		}
		System.out.println();
	}

	private void pattern_M() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {

				if (col == 1 || col == 7 || (col == row && row <= 4) || (col + row == 8 && row <= 4)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}

			}
			System.out.println();

		}
		System.out.println();
	}

	private void pattern_A_2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {

				if ((row + col == 5) || (col - row == 3) || (col == 1 && row > 3) || (col == 7 && row > 3)
						|| row == 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}

			}
			System.out.println();

		}
		System.out.println();
	}

	private void pattern_A() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || col == 7 || row == 4 || row == 1) {
					if ((row == 1 && col == 7) || (row == 1 && col == 1)) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}

				} else {
					System.out.print("  ");
				}

			}
			System.out.println();
		}

	}

	private void pattern_H() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || col == 7 || row == 4) {
					System.out.print("* ");

				} else {
					System.out.print("  ");
				}

			}
			System.out.println();

		}
		System.out.println();
	}

	private void pattern_V() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col && col <= 4 || col + row == 8 && row <= 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_Y_2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col && col <= 4 || col + row == 8 && row <= 4 || col == 4 && row > 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_Y() {

		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col && col <= 4 || col + row == 8) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	private void pattern_X() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col || col + row == 8) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();

	}

	private void pattern_N() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || col == 7 || row == col) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}
		System.out.println();
	}

	private void pattern_R_2() {
		pattern_D();
		for (int row = 2; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col == 7) {
					if (row == 1 && col == 7) {
						System.out.print("  ");
					} else if (row == 7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}

	}

	private void pattern_R() {
		pattern_D();
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || row == col) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}

	}

	private void pattern_B() {
		pattern_D();
		for (int row = 2; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col == 7) {
					if (row == 1 && col == 7) {
						System.out.print("  ");
					} else if (row == 7 && col == 7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}
		System.out.println();

	}

	private void pattern_P() {
		pattern_D();
		for (int row = 1; row <= 7; row++) {
			System.out.println("* ");
		}
		System.out.println();
	}

	private void pattern_D() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col == 7) {
					if (row == 1 && col == 7) {
						System.out.print("  ");
					} else if (row == 7 && col == 7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}
		System.out.println();

	}

	private void pattern_O() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col == 7) {
					if (row == 1 && (col == 1 || col == 7)) {
						System.out.print("  ");
					} else if (row == 7 && (col == 1 || col == 7)) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}
		System.out.println();

	}

	private void pattern_C() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1) {
					if (row == 1 && col == 1) {
						System.out.print("  ");
					} else if (row == 7 && col == 1) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}
		System.out.println();

	}

	private void pattern_I() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}

		System.out.println();
	}

	private void pattern2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7) {
					System.out.print("* ");
				} else {
					System.out.print("# ");
				}
			}
			System.out.println();

		}
		System.out.println();

	}

	private void pattern1() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				System.out.print("# ");
			}
			System.out.println();

		}
		System.out.println();
	}

}
