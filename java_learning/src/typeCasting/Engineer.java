package typeCasting;

public class Engineer extends Student {

	public static void main(String[] args) {
		Engineer eng = new Engineer();
		eng.doProjects();
		eng.study();
		String name = "abcd";
		System.out.println(name.hashCode());
//		System.out.println(eng.hashCode());//hashcode returns 123
		Student s = (Student)eng; //upCasting or widen casting.
//		System.out.println(s);
//		Student ss = new Student();//class exception.
		Student ss = new Engineer();
		Engineer en = (Engineer) ss;//down casting or narrow casting.
//		System.out.println(en); 
		en.doProjects();
		en.study();
		eng.shift();
	}
	private void shift() {
		int no = 2;
		System.out.println("shift operator left---->"+(no<<1));
//		System.out.println("shift operator right---->"+(no>>0));
		
	}
	public int hashCode() {  //it returns the eng object hashcode.
		return 123;
	}
	public void doProjects(){
		System.out.println("doing project");
	}

}
