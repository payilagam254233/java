package typeCasting;

public class TypeCastingDemo {

	public static void main(String[] args) {
		float price = 102.8f;
		int price1 = (int)price;
//		System.out.println(price);
//		System.out.println(price1);
		
		float ff = (float) 123.45;
//		System.out.println(ff);
		
		int ch = 'A';
//		System.out.println(ch);//printing husky value.
//		System.out.println((char)ch);//printing that character.
		
	}

}
