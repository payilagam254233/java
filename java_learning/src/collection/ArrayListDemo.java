package collection;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		//al.add(al);
		al.add("yogeshwaran");
		al.add("santhosh");
		al.add(10);
		al.add(true);
		al.add(5.4f);
		System.out.println(al);
		al.add("sownesh");
		al.add("vignesh");
		System.out.println(al);
		al.add("rasika");
		System.out.println(al);
		System.out.println(al.contains("yogeshwaran"));
		System.out.println(al.contains("selvi"));
		System.out.println(al.get(0));
		System.out.println(al.indexOf(true));
		System.out.println(al.lastIndexOf("sownesh"));
		System.out.println(al.remove(0));
		System.out.println(al);
		
		
		ArrayList al1 = new ArrayList();
		al1.addAll(al);
		System.out.println("contians--->"+al1.containsAll(al));
		System.out.println(al1);
		al1.add("eashwar");
		System.out.println("al1--->"+al1);
		//System.out.println(al1.retainAll(al));
		//System.out.println(al1);
		System.out.println(al1.removeAll(al));
		System.out.println(al1);
		


		
		

		// int no = 10;
		// Integer i =no;//autoboxing.
		// int no1= i;//autounboxing.

//(------------------wrapped class----------------)		
		// byte=Byte
		// int=Integer
		// short=Short
		// long=Long
		// float=Float
		// double=Double
		// boolean=Boolean
		// char=Character
//(------------------------------------------------)		

	}

}
