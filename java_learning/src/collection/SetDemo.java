package collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		ArrayList names = new ArrayList();
		names.add("yogeshwaran");
		names.add("yogeshwaran");
		names.add("santhosh");
		names.add("santhosh");
		names.add("sownesh");
		names.add("sownesh");
		names.add("vignesh");
		names.add("vignesh");
		names.add("aravind");
		names.add("aravind");
//		System.out.println(names);
		HashSet hs = new HashSet(names);
//		System.out.println(hs);

		/*
		 * for (Object obj : hs) { String str = (String) obj; if (str.startsWith("y")) {
		 * System.out.println(obj); } }
		 */

		/*
		 * Iterator i = names.iterator(); boolean result = i.hasNext(); while (result =
		 * i.hasNext()) { System.out.println(i.next()); }
		 */
		
		LinkedHashSet ls = new LinkedHashSet();
		ls.add("yogeshwaran");
		ls.add("vignesh");
		ls.add("sownesh");
		ls.add("santhosh");
		ls.add("rasika");
		//System.out.println(ls);
//		ordered by insertion
		TreeSet ts = new TreeSet();
		ts.add("yogeshwaran");
		ts.add("vignesh");
		ts.add("sownesh");
		ts.add("santhosh");
		ts.add("rasika");
//		ts.add(10);
//		ts.add(true); // ClassCastException in TreeSet same dataype should be insertion only workss and it will comes in natural order like ascending.
		System.out.println(ts.isEmpty());
		System.out.println(ts.getClass());
		System.out.println(ts);

	}

}
