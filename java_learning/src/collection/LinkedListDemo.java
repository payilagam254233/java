package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		LinkedList ll = new LinkedList();
		ll.add("yogeshwaran");
		ll.add("santhosh");
		ll.add("sownesh");
		ll.add(21);
		ll.add(22);
		ll.add(25);
		System.out.println(ll);
		ll.add("eashwar");
		ll.add(21);
		System.out.println(ll);
		System.out.println(ll.contains("contains--->"+"santhosh"));
		System.out.println(ll.getClass());
//		System.out.println(ll.get(0));
		System.out.println(ll.getFirst());
		System.out.println(ll.getLast());
		System.out.println(ll.indexOf(21));
		System.out.println(ll.lastIndexOf("santhosh"));
		System.out.println(ll.remove(0));
		System.out.println(ll.remove(2));
		System.out.println(ll);
		
		ArrayList al = new ArrayList();
		al.addAll(ll);
		System.out.println("al--->"+al);
		System.out.println("containsAll--->"+al.containsAll(ll));
		al.add("yogeshwaran");
		//System.out.println(al);
		//System.out.println(al.retainAll(ll));
		System.out.println("al--->"+al);
		al.removeAll(ll);
		System.out.println(al);
		
		
		ArrayList<String> names = new ArrayList();
		names.add("Aravind");
		names.add("Vignesh");
		names.add("Yogesh");
		names.add("Santhosh");
		names.add(2,"Sownesh");
		System.out.println(names);
		Collections.sort(names);
		System.out.println(names);
		
	}

}
