package collection;

import java.util.ArrayList;
import java.util.Collections;

public class Demo {

	public static void main(String[] args) {
//		ArrayList names = new ArrayList<>();
//		names.add("yogeshwaran");
//		names.add("santhosh");
//		names.add("sownesh");
//		names.add("vignesh");
//		names.add("aravind");
		
		/*
		 * Object[] ar = names.toArray(); for (Object obj : ar) {
		 * System.out.println(obj); }
		 */
//		ComparatorDemo cd = new ComparatorDemo();
//		System.out.println("before sort --->"+names);
//		Collections.sort(names,cd);
//		System.out.println("after sort by ascending or descending --->"+names);

		ArrayList al=new ArrayList();
		al.add("rajesh");
		al.add("kuppusami");
		al.add("munnusami");
		al.add("subbu");
		al.add("raina");
		
		/*
		 * Object[] ar = al.toArray(); for (Object obj : ar) { System.out.println(obj);
		 * }
		 */
		ComparatorDemo cd = new ComparatorDemo();
		System.out.println("before--->"+al);
		Collections.sort(al,cd);
		System.out.println("after--->"+al);

	}

}
