package collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		MapDemo md = new MapDemo();
		md.hashmap();
		md.linkedhashmap();
		md.treemap();

	}

	private void treemap() {
		
		
	}

	private void linkedhashmap() {
		
		
		
	}

	private void hashmap() {
		HashMap hm = new HashMap();
		hm.put("ChickenBriyani", 120);
		hm.put("MuttonBriyani", 150);
		hm.put("BeefBiriyani", 110);
		hm.put("ChickenRice", 110);
		System.out.println(hm);
//		hm.put("ChickenRice", 120);//so it can upadate the key value.		
		System.out.println(hm.get("ChickenRice"));
//		System.out.println(hm.remove("BeefBiriyani"));
		System.out.println(hm.containsKey("BeefBiriyani"));
		System.out.println(hm.containsValue(110));
//		System.out.println(hm.putIfAbsent("BeefBiriyani", 110));

		System.out.println(hm);

//Key--> ChickenBriyani  MuttonBriyani  ChickenRice -->KeySet
//Value--->120  150  110-->sometimes its Set or List(noramally aslo called collection.
	
		//Key                value      Key-Value
		//ChickenBriyani     120		Entry Value
		//MuttonBriyani		 150		Entry Value
		//ChickenRice		 110		Entry Value
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		System.out.println(hm.entrySet());
		Iterator i = hm.entrySet().iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
			Map.Entry me = (Map.Entry)i.next();//down casting
			System.out.println(me.getKey());
			System.out.println(me.getValue());
			me.setValue((int)me.getValue()+5);
			
		}
		System.out.println(hm);
	
	}

}
