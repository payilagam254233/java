package collection;

public class Samsung implements Comparable<Samsung> {

	public int price, ram, mp;

	public Samsung(int price, int ram, int mp) {
		this.price = price;
		this.ram = ram;
		this.mp = mp;

	}

	public static void main(String[] args) {
		Samsung s1 = new Samsung(17000, 8, 24);
		Samsung s2 = new Samsung(15000, 6, 12);
		int result = s1.compareTo(s2);
		System.out.println(result);
		if (result > 0) {
			System.out.println("s1 price is high");
		} else if (result < 0) {
			System.out.println("s2 price is high");
		} else {
			System.out.println("s1,s2 are equal");
		}

	}

	
	public int compareTo(Samsung s2) {
		if (this.price > s2.price) {
			return +2234432;
		} else if (this.price < s2.price) {
			return -233;
		}
		return 0;
	}

}