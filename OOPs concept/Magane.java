public class Magane extends Naina
{
public static void main(String[] args)
{
Naina magan = new Magane(); //dynamic binding or late binding
Magane magan1 = new Magane(); //static binding or early binding
magan.motivate();
magan.workhard();
magan.study();
//magan.play(); //there is no method in parent class 

}
public void study()
{
System.out.println("Magane---->study");
}
public void play()
{
System.out.println("Magane----->play");	
}
}
