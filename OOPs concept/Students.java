public class Students
{
//non static variable
String name;
int roll_no;
public Students(String name,int roll_no)      //one parameter const
{
	this.name = name;
	this.roll_no = roll_no;

}

public Students(Students stud1)      //one parameter const
{
	this.name = stud1.name;
	this.roll_no = stud1.roll_no;

}

//main method
public static void main(String[] args)
{
	Students stud1 = new Students("yogeshwaran",212);
	Students stud2 = new Students(stud1);
	Students stud3 = new Students("eashwar",116);
	Students stud4 = new Students(stud3);
	Students stud5 = new Students("sathosh",120);
	Students stud6 = new Students(stud5);

	stud1.details();
	stud2.details();
	stud3.details();
	stud4.details();
	stud5.details();
	stud6.details();
}
public void details()
{
	System.out.println(name);
	System.out.println(roll_no);

		
}
}