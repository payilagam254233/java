public class Son extends Mom
{
public Son()
{
super();
System.out.println("zero argument constructor-son");	
}
public Son(int age)
{
super(10);
System.out.println("one argument constructor-son");	
}
public Son(String name)
{
super("name");
}
public Son(double name)
{
super(50.5);
}
public Son(char CHAR)
{
super('A');
}

public static void main(String[] args)
{
Son ms = new Son('A');
Son ms1 = new Son(50.5);
Son ms2 = new Son("name");
}
}